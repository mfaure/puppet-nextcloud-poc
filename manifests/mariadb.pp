# @summary Install Mariadb and create database
#
# Install Mariadb and create database
#
# @example
#   include nextcloud::mariadb
class nextcloud::mariadb {

  # Install server + client + bindings
  class { 'mysql::server':
    package_name            => 'mariadb-server',
    root_password           => $nextcloud::db_root_password,
    remove_default_accounts => true,
    override_options        => {
      'mysqld' => {
        'innodb_file_format'    => 'Barracuda',
        'innodb_file_per_table' => '1',
        'innodb_large_prefix'   => true,
      }
    },
  }

  class { 'mysql::client':
    package_name => 'mariadb-client'
  }
  class { 'mysql::bindings':
    php_enable => true,
  }

  # Create database for Nextcloud
  mysql::db { $nextcloud::db_database_name:
    user     => $nextcloud::db_username,
    password => $nextcloud::db_password,
    host     => $nextcloud::db_hostname,
    grant    => ['ALL'],
  }
}
