# @summary Install NextCloud
#
# Install NextCloud
#
# @param nc_admin_username Username of NextCloud administrator
# @param nc_admin_password Password of NextCloud administrator
# @param nc_install_parent_dir
#     Absolute directory where NextCloud archive will be extracted. By design, a 'nextcloud' directory is created
# @param nc_data_parent_dir
#     Absolute directory where NextCloud data will be stored. A `data/` subdir will be created
# @param manage_apache Whether to install Apache
# @param apache_hostname If manage_apache: Hostname for the virtual host to be create
# @param apache_port If manage_apache: port Apache will listen to
# @param apache_default_vhost Define the created vhost as the default one
# @param php_version If manage_apache: PHP version to install
# @param manage_redis Whether to install Redis
# @param manage_mariadb Whether to install MariaDB
# @param db_hostname FQDN or IP of MariaDB host
# @param db_port Port to connect to MariaDB host
# @param db_root_password Password to give to MariaDB root user
# @param db_username MariaDB user that will be created and grant access to the DB
# @param db_password Password for MariaDB user that will connect to the DB
# @param db_database_name Name of the database that will be created and used for NextCloud
# @param db_table_prefix String to prefix tables name
#
# @example
#   include nextcloud
class nextcloud (
  String                $db_password,
  String                $nc_admin_username,
  String                $nc_admin_password,
  Stdlib::Absolutepath  $nc_install_parent_dir = '/var/www',
  Stdlib::Absolutepath  $nc_data_parent_dir    = '/var/lib/nextcloud',
  String                $nc_version            = 'latest',
  Boolean               $manage_apache         = false,
  Stdlib::Host          $apache_hostname       = 'nextcloud.example.org',
  Stdlib::Port          $apache_port           = 80,
  Boolean               $apache_default_vhost  = true,
  String                $apache_user           = 'www',
  String                $php_version           = '7.2',
  Boolean               $manage_redis          = false,
  Boolean               $manage_mariadb        = false,
  Stdlib::Host          $db_hostname           = 'localhost',
  Stdlib::Port          $db_port               = 3306,
  String                $db_root_password      = 'StrongPa$$w0rdz',
  String                $db_username           = 'mariadb',
  String                $db_database_name      = 'nextcloud',
  String                $db_table_prefix       = '',
) {

  # Local variable
  $nc_download_url_base = 'https://download.nextcloud.com/server/releases'
  # TODO manage possible formats of $nextcloud_version: 'latest.tar.bz2', 'nextcloud-16.0.5.tar.bz2'...
  $nc_archive = "${nc_version}.tar.bz2"
  $nc_archive_url = "${nc_download_url_base}/${nc_archive}"
  $nc_install_dir = "${nc_install_parent_dir}/nextcloud" # hardcoded because archive extracts in a 'nextcloud' dir
  $nc_data_dir = "${nc_data_parent_dir}/data"

  # Install Apache + PHP-FPM and configure vhost
  if $manage_apache {
    include nextcloud::apache
  }

  # Install Redis
  if $manage_redis {
    include nextcloud::redis
  }

  # Install MariaDB
  if $manage_mariadb {
    include nextcloud::mariadb
  }

  # Create NextCloud directory
  file { $nc_install_parent_dir:
    ensure => directory,
  }
  -> file { $nc_install_dir:
    ensure => directory,
    owner  => $apache_user,
    group  => $apache_user,
    mode   => '0755',
  }

  # Create NextCloud *data* directory
  file { $nc_data_parent_dir:
    ensure => directory,
  }
  -> file { $nc_data_dir:
    ensure => directory,
    owner  => $apache_user,
    group  => $apache_user,
    mode   => '0755',
  }

  # Get Nextcloud archive
  archive { 'download-nextcloud-archive':
    ensure       => 'present',
    path         => "/tmp/${nc_archive}",
    source       => $nc_archive_url,
    extract      => true,
    extract_path => "${nc_install_parent_dir}/",
    user         => $apache_user,
    group        => $apache_user,
    require      => File[$nc_install_dir]
    # TODO add checksum verification
  }

  # Nextcloud autoconfig (unattended configuration)
  file { "${nc_install_dir}/config/autoconfig.php":
    ensure  => present,
    owner   => $apache_user,
    group   => $apache_user,
    mode    => '0640',
    content => template('nextcloud/autoconfig.php.erb'),
  }

  # TODO configure APCu + Redis in NextCloud.
  # See https://docs.nextcloud.com/server/16/admin_manual/configuration_server/caching_configuration.html#id1
}
