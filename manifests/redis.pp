# @summary Install Redis
#
# Install Redis
#
# @example
#   include nextcloud::redis
class nextcloud::redis {
  include redis
}
