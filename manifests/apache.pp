# @summary Install Apache, PHP-FPM and PHP extensions required for Nextcloud
#
# Install Apache, PHP-FPM and PHP extensions required for Nextcloud
#
# @example
#   include nextcloud::apache
class nextcloud::apache {
  $_fastcgi_socket = 'fcgi://127.0.0.1:9000'
  # ProxyPassMatch: see https://httpd.apache.org/docs/2.4/mod/mod_proxy_fcgi.html#examples
  $_my_custom_fragment = "ProxyPassMatch ^/.*\\.php(/.*)?\$ ${_fastcgi_socket}${nextcloud::nc_install_dir}/ enablereuse=on"

  # PHP + modules
  class { 'php::globals':
    php_version => $nextcloud::php_version,
  }
  -> class { 'php':
    extensions => {
      apcu      => {},
      bz2       => {},
      ctype     => {},
      curl      => {},
      dom       => {},
      # exif      => {}, # already installed
      gd        => {},
      iconv     => {},
      imagick   => {},
      intl      => {},
      json      => {},
      # fileinfo  => {}, # already installed
      # libxml    => {}, # already installed
      mbstring  => {},
      # openssl   => {}, # already installed
      mysql     => {},
      posix     => {},
      # session   => {}, # already installed
      simplexml => {},
      xmlreader => {},
      xmlwriter => {},
      zip       => {},
      # zlib      => {}, # already installed
    },
  }

  # Apache
  class { 'apache::mod::proxy': }
  class { 'apache::mod::proxy_fcgi': }
  apache::vhost { $nextcloud::apache_hostname:
    docroot         => $nextcloud::nc_install_dir,
    port            => $nextcloud::apache_port,
    default_vhost   => $nextcloud::apache_default_vhost,
    override        => 'all',
    custom_fragment => $_my_custom_fragment,
    require         => Class[php],
  }
}
