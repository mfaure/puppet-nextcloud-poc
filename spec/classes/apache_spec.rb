require 'spec_helper'

describe 'nextcloud::apache' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:pre_condition) do
        "
        class { 'nextcloud':
          db_password       => 'S3cr#TPassw0rdz',
          nc_admin_username => 'ncadmin',
          nc_admin_password => 'ncadminPassword',
          manage_apache     => true,
        }
        "
      end

      it { is_expected.to compile }
      it { is_expected.to contain_class('php') }
      it { is_expected.to contain_class('apache::mod::proxy') }
      it { is_expected.to contain_class('apache::mod::proxy_fcgi') }
      it { is_expected.to contain_apache__vhost('nextcloud.example.org') }
    end
  end
end
