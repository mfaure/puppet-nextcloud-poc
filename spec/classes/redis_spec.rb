require 'spec_helper'

describe 'nextcloud::redis' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:pre_condition) do
        "
        class { 'nextcloud':
          db_password       => 'S3cr#TPassw0rdz',
          nc_admin_username => 'ncadmin',
          nc_admin_password => 'ncadminPassword',
          manage_redis      => true,
        }
        "
      end

      it { is_expected.to compile }
      it do
        is_expected.to contain_service('redis-server')
          .with_enable(true)
          .with_ensure('running')
      end
    end
  end
end
