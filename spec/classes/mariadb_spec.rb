require 'spec_helper'

describe 'nextcloud::mariadb' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) do
        os_facts.merge(root_home: '/root') # merging custom fact "root_home" is required for mariadb
      end

      let(:pre_condition) do
        "
        class { 'nextcloud':
          db_password       => 'S3cr#TPassw0rdz',
          nc_admin_username => 'ncadmin',
          nc_admin_password => 'ncadminPassword',
          manage_mariadb      => true,
        }
        "
      end

      it { is_expected.to compile }
      it { is_expected.to contain_class('mysql::server') }
      it { is_expected.to contain_class('mysql::client') }
      it { is_expected.to contain_class('mysql::bindings') }
      it { is_expected.to contain_mysql__db('nextcloud') }
      # As unit-test check the Puppet Catalog and not the host, it is OK to have 'mysql-server' as package name
      # and not 'mariabd'.
      it { is_expected.to contain_package('mysql-server') }
      it { is_expected.to contain_service('mysqld') }
    end
  end
end
