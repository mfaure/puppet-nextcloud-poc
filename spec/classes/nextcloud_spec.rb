require 'spec_helper'

describe 'nextcloud' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          db_password: 'S3cr#TPassw0rdz',
          nc_admin_username: 'ncadmin',
          nc_admin_password: 'ncadminPassword',
        }
      end

      it { is_expected.to compile }
    end
  end
end
