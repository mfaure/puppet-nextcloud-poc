require 'spec_helper_acceptance'

describe 'NextCloud class' do
  context 'with default parameters' do
    pp = <<-EOS
      class { 'nextcloud':
        db_password       => 'S3cr#TPassw0rdz',
        nc_admin_username => 'ncadmin',
        nc_admin_password => 'ncadminPassword',
      }
    EOS
    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    # TODO: once actual code is created in the module, add tests here with `describe` and expectations
  end

  context 'with managed_apache=true' do
    pp = <<-EOS
      class { 'nextcloud':
        db_password       => 'S3cr#TPassw0rdz',
        nc_admin_username => 'ncadmin',
        nc_admin_password => 'ncadminPassword',
        manage_apache     => true,
      }
    EOS

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    # Following test disabled ; waiting for upstream feedback
    # See https://github.com/voxpupuli/puppet-php/issues/548
    # it 'applies idempotently' do
    #   apply_manifest(pp, catch_changes: true)
    # end

    describe service('php7.2-fpm') do
      it { is_expected.to be_running }
    end
    describe service('apache2') do
      it { is_expected.to be_running }
    end
    describe file('/var/www/nextcloud/index.php') do
      it { is_expected.to be_file }
    end
    describe file('/var/www/nextcloud/core/') do
      it { is_expected.to be_directory }
    end

    # TODO: once actual code is created in the module, add tests here with `describe` and expectations
  end

  # /!\ Commented out as puppet-redis does not support Ubuntu 18.04 yet (as of 2019-10-06)
  #
  # context 'with managed_redis=true' do
  #   pp = <<-EOS
  #     class { 'nextcloud':
  #       db_password       => 'S3cr#TPassw0rdz',
  #       nc_admin_username => 'ncadmin',
  #       nc_admin_password => 'ncadminPassword',
  #       manage_redis      => true,
  #     }
  #   EOS
  #
  #   it 'applies without error' do
  #     apply_manifest(pp, catch_failures: true)
  #   end
  #   it 'applies idempotently' do
  #     apply_manifest(pp, catch_changes: true)
  #   end
  #
  #   # TODO: once actual code is created in the module, add tests here with `describe` and expectations
  # end

  context 'with managed_mariadb=true' do
    pp = <<-EOS
      class { 'nextcloud':
        db_password       => 'S3cr#TPassw0rdz',
        nc_admin_username => 'ncadmin',
        nc_admin_password => 'ncadminPassword',
        manage_mariadb    => true,
      }
    EOS

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe service('mariadb') do
      it { is_expected.to be_running }
    end

    # TODO: once actual code is created in the module, add tests here with `describe` and expectations
  end
end
