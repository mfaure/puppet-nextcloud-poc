require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'
install_module
install_module_dependencies

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # we need :
    # * an Apache webserver with PHP
    # * a Redis server
    # * a MariaDB server

    hosts.each do |host|
      on host, puppet('module', 'install', 'puppetlabs-stdlib')
      on host, puppet('module', 'install', 'puppetlabs-apache') # This should be move into acceptance/nextcloud_spec.rb
      on host, puppet('module', 'install', 'puppet-php')        # This should be move into acceptance/nextcloud_spec.rb
      on host, puppet('module', 'install', 'puppet-redis')      # This should be move into acceptance/nextcloud_spec.rb
      on host, puppet('module', 'install', 'puppetlabs-mysql')  # This should be move into acceptance/nextcloud_spec.rb
    end
  end
end
