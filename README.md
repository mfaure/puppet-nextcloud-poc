# Nextcloud

Install and configure NextCloud.

## Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with nextcloud](#setup)
    * [What nextcloud affects](#what-nextcloud-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nextcloud](#beginning-with-nextcloud)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module installs and configures NextCloud. If specified, it also installs Apache and PHP.

## Setup

### What nextcloud affects **OPTIONAL**

If it's obvious what your module touches, you can skip this section. For example, folks can probably figure out that your mysql_instance module affects their MySQL instances.

If there's more that they should know about, though, this is the place to mention:

* Files, packages, services, or operations that the module will alter, impact, or execute.
* Dependencies that your module automatically installs.
* Warnings or other important notices.

### Setup Requirements 

Hard dependencies:

* puppetlabs/stdlib >= 4.13.1
* puppet/archive >= 4.2.0

Soft dependencies:

* if `$manage_apache` is true:
    * `puppetlabs-apache` >= 5.0.0
    * `puppet-php` >= 6.0.0
* if `$manage_redis` is true:
    * `puppet-redis` >= 4.2.0
* if `$manage_mariadb` is true:
    * `puppetlabs-mysql` >= 10.2.0

### Beginning with nextcloud

The very basic steps needed for a user to get the module up and running. This can include setup steps, if necessary, or it can be an example of the most basic use of the module.

## Usage

Include usage examples for common use cases in the **Usage** section. Show your users how to use your module to solve problems, and be sure to include code examples. Include three to five examples of the most important or common tasks a user can accomplish with your module. Show users how to accomplish more complex tasks that involve different types, classes, and functions working in tandem.

## Reference

This section is deprecated. Instead, add reference information to your code as Puppet Strings comments, and then use Strings to generate a REFERENCE.md in your module. For details on how to add code comments and generate documentation with Strings, see the Puppet Strings [documentation](https://puppet.com/docs/puppet/latest/puppet_strings.html) and [style guide](https://puppet.com/docs/puppet/latest/puppet_strings_style.html)

If you aren't ready to use Strings yet, manually create a REFERENCE.md in the root of your module directory and list out each of your module's classes, defined types, facts, functions, Puppet tasks, task plans, and resource types and providers, along with the parameters for each.

For each element (class, defined type, function, and so on), list:

  * The data type, if applicable.
  * A description of what the element does.
  * Valid values, if the data type doesn't make it obvious.
  * Default value, if any.

For example:

```
### `pet::cat`

#### Parameters

##### `meow`

Enables vocalization in your cat. Valid options: 'string'.

Default: 'medium-loud'.
```

## Limitations

* Supported platforms is limited to Ubuntu 18.04 (for now). 
* Redis is meant to be on the host Apache and NextCloud application are installed. 

Feel free to submit Merge Requests to enhance those limitations.

## Development

In the Development section, tell other users the ground rules for contributing to your project and how they should submit their work.

## Release Notes/Contributors/Etc. **Optional**

If you aren't using changelog, put your release notes here (though you should consider using changelog). You can also add any additional sections you feel are necessary or important to include here. Please use the `## ` header.
